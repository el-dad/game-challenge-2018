//
//  CandleLight.swift
//  Senses
//
//  Created by Rafael Forbeck on 18/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

class CandleLight {
    
    private var lightNode: SKLightNode
    private var lastUpdateLight: TimeInterval = 0
    private var lightUpdateTime: TimeInterval = 0
    private var lightYPosition: CGFloat!
    private var extinguished = false
    
    init(lightNode: SKLightNode){
        self.lightNode = lightNode
        self.lightYPosition = lightNode.position.y
    }
    
    func update(deltaTime: TimeInterval) {
        if extinguished {
            let falloff = lightNode.falloff + 2 * CGFloat(deltaTime)
            lightNode.falloff = falloff
        } else {
            flutter(deltaTime: deltaTime)
        }
    }
    
    private func flutter(deltaTime: TimeInterval) {
        lastUpdateLight += deltaTime
        if lastUpdateLight > lightUpdateTime {
            lastUpdateLight = 0
            lightUpdateTime = TimeInterval.randomBetween(a: 0.05, b: 0.1)
            lightNode.falloff = CGFloat.randomBetween(a: 1.9, b: 2.1)
            lightNode.position.y = CGFloat.randomBetween(a: lightYPosition, b: lightYPosition + 1)
            lightNode.lightColor = UIColor(hue: 0.15, saturation: 0.2, brightness: 0.74, alpha: 1)
        }
    }
    
    func extinguish() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.extinguished = true
        })
    }
}
