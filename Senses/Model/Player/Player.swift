//
//  Player.swift
//  Senses
//
//  Created by Rafael Forbeck on 17/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit
import AVFoundation

class Player: SKNode, AVAudioPlayerDelegate {

    private var directionTarget: CGFloat!
    private var rotationDirection = CGFloat(1)
    private var light: CandleLight!
    private var moving = false
    private var finished = false
    private var stepsSound: AVAudioPlayer!
    private var waterStepsSound: AVAudioPlayer!
    private var runningStepsSound: AVAudioPlayer!
    private var stepVolume = Float(0)
    private var maxStepVolume = Float(0.1)
    private var walkingOnWater = false
    private var idleAnimation : SKAction!
    private var walkAnimation : SKAction!
    private var runAnimation : SKAction!
    private var playerSprite: SKSpriteNode!
    private var running = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.directionTarget = self.zRotation
        let lightNode = childNode(withName: "Light") as! SKLightNode
        self.light = CandleLight(lightNode: lightNode)
        setupStepSound()
        setupAnimations()
    }
    
    private func setupStepSound() {
        stepsSound = AudioPlayer.shared.loadSound(fileName: "steps1.mp3")
        stepsSound.volume = 0
        stepsSound.play()
        stepsSound.delegate = self

        waterStepsSound = AudioPlayer.shared.loadSound(fileName: "water_steps.wav")
        waterStepsSound.volume = 0
        waterStepsSound.numberOfLoops = -1
        waterStepsSound.play()
        
        runningStepsSound = AudioPlayer.shared.loadSound(fileName: "running.wav")
        runningStepsSound.volume = 0
        runningStepsSound.numberOfLoops = -1
        runningStepsSound.play()
    }
    
    func setupAnimations() {
        
        let idleAnimatedAtlas = SKTextureAtlas(named: "Idle")
        var idleFrames = [SKTexture]()
        
        let numIdleImages = idleAnimatedAtlas.textureNames.count
        for i in 1..<numIdleImages + 1 {
            let idleTextureName = "img_char_idle_0\(i)"
            idleFrames.append(idleAnimatedAtlas.textureNamed(idleTextureName))
        }
        let firstIdleFrame = idleFrames[0]
        playerSprite = SKSpriteNode(texture: firstIdleFrame)
        playerSprite.zRotation = -CGFloat.pi / 2
        playerSprite.setScale(0.35)
        addChild(playerSprite)
        idleAnimation = SKAction.repeatForever(
            SKAction.animate(with: idleFrames,
                             timePerFrame: 0.15,
                             resize: false,
                             restore: true))
        
        playerSprite.run(idleAnimation)
        
        let walkAnimatedAtlas = SKTextureAtlas(named: "Walk")
        var walkFrames = [SKTexture]()
        
        let numWalkImages = walkAnimatedAtlas.textureNames.count
        for i in 1..<numWalkImages + 1 {
            let walkTextureName = "img_char_walk_0\(i)"
            walkFrames.append(walkAnimatedAtlas.textureNamed(walkTextureName))
        }

        walkAnimation = SKAction.repeatForever(
            SKAction.animate(with: walkFrames,
                             timePerFrame: 0.15,
                             resize: false,
                             restore: true))
        
        runAnimation = SKAction.repeatForever(
            SKAction.animate(with: walkFrames,
                             timePerFrame: 0.075,
                             resize: false,
                             restore: true))
        
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        if isPaused {
            return
        }
        startSteps()
    }
    
    func startSteps() {
        let soundNumber = arc4random_uniform(3) + 1
        stepsSound = AudioPlayer.shared.loadSound(fileName: "steps\(soundNumber).mp3")
        stepsSound.delegate = self
        stepsSound.volume = 0
        stepsSound.play()
    }
    
    func setupPhysicBody() {
        let body = SKPhysicsBody(circleOfRadius: 25, center: CGPoint(x: 5,y: -5))
        body.isDynamic = true
        body.affectedByGravity = false
        body.friction = 0
        body.restitution = 0
        body.linearDamping = 2.5
        body.angularDamping = 0.1
        body.mass = 1
        self.physicsBody = body
    }
    
    func move(inDirection point: CGPoint, run: Bool) {
        running = run
        if run {
            playerSprite.run(runAnimation)
        } else {
            playerSprite.run(walkAnimation)
        }
        self.move(inDirection: point)
    }
        
    func move(inDirection point: CGPoint) {
        
        moving = true
        self.directionTarget = (point - self.position).direction()
        calculateRotationDirection()
    }
    
    func stop() {
        playerSprite.run(idleAnimation)
        running = false
        moving = false
    }
    
    func update(deltaTime: TimeInterval) {
        if !finished {
            if moving {
                stepVolume = maxStepVolume
                updatePosition(deltaTime: deltaTime)
            } else {
                stepVolume -= Float(deltaTime) * 0.2
                stepVolume = stepVolume < 0.0 ? 0.0 : stepVolume
            }
            if running {
                waterStepsSound.volume = 0
                stepsSound.volume = 0
                runningStepsSound.volume = stepVolume
            } else if walkingOnWater {
                stepsSound.volume = 0
                runningStepsSound.volume = 0
                waterStepsSound.volume = stepVolume * 0.3
            } else {
                waterStepsSound.volume = 0
                runningStepsSound.volume = 0
                stepsSound.volume = stepVolume
            }
            updateDirection(deltaTime: deltaTime)
        }
        light.update(deltaTime: deltaTime)
    }
    
    func calculateRotationDirection() {
        
        let angleBetween = CGFloat.shortestAngleBetween(self.zRotation, angle2: directionTarget)
        if angleBetween < 0.0 {
            rotationDirection = -1
        } else {
            rotationDirection = 1
        }
    }
    
    private func updateDirection(deltaTime: TimeInterval) {
        if abs(self.zRotation - directionTarget) < 0.1 {
            return
        }
        var newDirection = self.zRotation + Config.playerAngularVelocity * CGFloat(deltaTime) * rotationDirection

        if rotationDirection > 0.0 {
            if newDirection > CGFloat.pi {
                newDirection -= CGFloat.pi * 2
            }
            if newDirection * directionTarget > 0.0 && newDirection > directionTarget{
                newDirection = directionTarget
            }
        } else {
            if newDirection < -CGFloat.pi {
                newDirection += CGFloat.pi * 2
            }
            if newDirection * directionTarget > 0.0 && newDirection < directionTarget{
                newDirection = directionTarget
            }
        }
        
        self.zRotation = newDirection
    }
    
    private func updatePosition(deltaTime: TimeInterval) {
        var force: CGPoint
        if running {
            force = CGPoint.newVector(withAngle: directionTarget) * Config.playerRunForce
        } else {
            force = CGPoint.newVector(withAngle: directionTarget) * Config.playerMoveForce
        }
        
        self.physicsBody?.applyForce(CGVector(dx: force.x, dy: force.y))
    }
    
    func walkOnWater(isWalkingOnWater: Bool) {
        walkingOnWater = isWalkingOnWater
    }
    
    func finish() {
        finished = true
        stepsSound.stop()
        waterStepsSound.stop()
        runningStepsSound.stop()
        light.extinguish()
    }
    
    func pause(pause: Bool) {
        if pause {
            stepsSound.stop()
            waterStepsSound.stop()
            runningStepsSound.stop()
        } else {
            stepsSound.play()
            waterStepsSound.play()
            runningStepsSound.play()
        }
    }
}
