import Foundation

class AppSingleton {
    public static let instance = AppSingleton()
    public var colorConfusionDictionary = [
        "sound-light-blue.mp3": "sound-light-green.mp3",
        "sound-light-green.mp3": "sound-light-yellow.mp3",
        "sound-light-red.mp3": "sound-light-blue.mp3",
        "sound-light-yellow.mp3": "sound-light-red.mp3"
    ]
    public func shuffleColors() {
        var originals = ["blue", "green", "red", "yellow"].makeIterator()
        var colors = ["blue", "green", "red", "yellow"]
        
        repeat {
            let color = colors.remove(at: Int(arc4random() % UInt32(colors.count)))
            let original = originals.next()!
            self.colorConfusionDictionary["sound-light-\(original).mp3"] = "sound-light-\(color).mp3"
            self.colorConfusionDictionary["sound-puzzle-\(original).mp3"] = "sound-puzzle-\(color).mp3"
        } while (colors.count > 0)
    }
}
