//
//  SoundEmitter.swift
//  Senses
//
//  Created by Rafael Forbeck on 18/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import AVFoundation
import SpriteKit

class SoundEmitter: SKNode {
    
    private var audioPlayer: AVAudioPlayer!
    private var playInLoop: Bool!
    private var volume: Float = 1
    private var interval: TimeInterval = 0.0
    var trigger: CGRect!
    var range: CGFloat!
    var isPlaying = false
    
    func setup(soundFileName: String, playInLoop: Bool) {
        self.audioPlayer = AudioPlayer.shared.loadSound(fileName: AppSingleton.instance.colorConfusionDictionary[soundFileName] ?? soundFileName)
        self.playInLoop = playInLoop
        let triggerNode = childNode(withName: "Trigger")!
        triggerNode.isHidden = true
        self.trigger = triggerNode.frame
        let rangeNode = childNode(withName: "Range")!
        rangeNode.isHidden = true
        self.range = CGFloat(max(Float(rangeNode.frame.width), Float(rangeNode.frame.height)) * 0.5)
        if userData!["Volume"] != nil {
            self.volume = userData!["Volume"] as! Float
        }
        if userData!["Interval"] != nil {
            self.interval = TimeInterval(userData!["Interval"] as! Float)
        }
        
        audioPlayer.volume = 0
        play()
    }
    
    func play() {
        if playInLoop {
            if interval == 0.0 {
                audioPlayer.numberOfLoops = -1
            } else {
                let nextPlayTime = audioPlayer.duration + interval
                DispatchQueue.main.asyncAfter(deadline: .now() + nextPlayTime, execute: {
                    if self.isPlaying {
                        self.play()
                    }
                })
            }
        } else {
            audioPlayer.numberOfLoops = 0
        }
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    func update(pan: Float, volume: Float) {
        isPlaying = true
        audioPlayer.pan = pan
        audioPlayer.volume = volume * self.volume
    }
    
    func turnDown() {
        isPlaying = false
        audioPlayer.volume = 0
    }
    
    func stop() {
        isPlaying = false
        audioPlayer.setVolume(0, fadeDuration: 1)
        audioPlayer.numberOfLoops = 0
    }
    
    func pause(pause: Bool) {
        if pause {
            audioPlayer.pause()
        } else {
            audioPlayer.play()
        }
    }
}
