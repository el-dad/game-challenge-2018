//
//  AudioPlayer.swift
//  Senses
//
//  Created by Rafael Forbeck on 18/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import AVFoundation

final class AudioPlayer {
    
    static var shared = AudioPlayer()
    
    private init() {
    }
    
    func loadSound(fileName: String) -> AVAudioPlayer {
        
        let path = Bundle.main.path(forResource: fileName, ofType: nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            let sound = try AVAudioPlayer(contentsOf: url)
            sound.prepareToPlay()
            return sound
        } catch {
            fatalError("Arquivo de som não encontrado")
        }
    }
}
