//
//  SoundsManager.swift
//  Senses
//
//  Created by Rafael Forbeck on 18/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

class SoundsManager {
    
    private var soundEmitters = [SoundEmitter]()
    
    func registerSoundEmitter(emitter: SoundEmitter, playInLoop: Bool) {
        guard let soundFileName = emitter.userData?[Config.userDataSounodFileName] as? String else {
            fatalError("Nome do arquivo de som inválido ou não informado.")
        }
        emitter.setup(soundFileName: soundFileName, playInLoop: playInLoop)
        soundEmitters.append(emitter)
    }
    
    func update(deltaTime: TimeInterval, playerPosition: CGPoint, playerDirection: CGFloat) {
        for emitter in soundEmitters {
            if emitter.trigger.contains(playerPosition - emitter.absolutPosition()) {
                let distance = (playerPosition - emitter.absolutPosition()).length()
                let volume = CGFloat(1 - distance / emitter.range).clamped(0, 1)
                let sinVolume = (sin(CGFloat.pi * (volume - 0.5)) + 1) / 2
                
                let soundDirection = emitter.absolutPosition() - playerPosition
                let soundAngle = CGFloat.shortestAngleBetween(playerDirection, angle2: soundDirection.direction())
                
                var pan: CGFloat
                if soundAngle > 0 {
                    if soundAngle < CGFloat.pi * 0.25 {
                        pan = soundAngle / (CGFloat.pi * 0.25)
                    } else if soundAngle < CGFloat.pi * 0.75 {
                        pan = 1
                    } else {
                        pan = (CGFloat.pi - soundAngle) / (CGFloat.pi * 0.25)
                    }
                } else {
                    if soundAngle > -CGFloat.pi * 0.25 {
                        pan = soundAngle / (CGFloat.pi * 0.25)
                    } else if soundAngle > -CGFloat.pi * 0.75 {
                        pan = -1
                    } else {
                        pan = (-CGFloat.pi - soundAngle) / (CGFloat.pi * 0.25)
                    }
                }
                emitter.update(pan: Float(-pan), volume: Float(sinVolume))
            } else {
                emitter.turnDown()
            }
        }
    }
    
    func finish() {
        for emitter in soundEmitters {
            emitter.stop()
        }
    }
    
    func pause(pause:Bool) {
        for sound in soundEmitters {
            sound.pause(pause: pause)
        }
    }
}

