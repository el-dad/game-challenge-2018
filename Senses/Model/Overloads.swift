//
//  Overloads.swift
//  Senses
//
//  Created by Rafael Forbeck on 17/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (left: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: left.x * scalar, y: left.y * scalar)
}

func / (left: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: left.x / scalar, y: left.y / scalar)
}

func == (left: SKColor, right: SKColor) -> Bool {
    return (left.ciColor.red == right.ciColor.red &&
            left.ciColor.green == right.ciColor.green &&
            left.ciColor.blue == right.ciColor.blue)
}

