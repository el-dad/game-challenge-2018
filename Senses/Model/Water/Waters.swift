//
//  Water.swift
//  Senses
//
//  Created by Rafael Forbeck on 08/11/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

class Waters {
    
    private var waterList = [SKSpriteNode]()
    
    func registerWater(water: SKSpriteNode) {
        waterList.append(water)
    }
    
    func isPlayerOnTheWater(playerPosition: CGPoint) -> Bool {
        for water in waterList {
            let absPosition = water.absolutPosition()
            if (absPosition - playerPosition).sqrtLength() < 10000 {
                return true
            }
        }
        return false
    }
}
