//
//  Enums.swift
//  Senses
//
//  Created by Rafael Forbeck on 18/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import Foundation

enum PuzzleLightColor: String {
    case Blue = "blue"
    case Green = "green"
    case Red = "red"
    case Yellow = "yellow"
}
