//
//  LightDisplay.swift
//  Senses
//
//  Created by Rafael Forbeck on 24/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

class LightDisplay: SKNode {
    
    private var emitter: SoundEmitter!
    private var lightColor: PuzzleLightColor!
    private var turnOffTime = TimeInterval(0.5)
    private var turnOnTime = TimeInterval(0.5)
    private var isOn = false
    private var currentTime = TimeInterval()
    private var light: SKLightNode!
    private var lightSprite: SKSpriteNode!
    
    func setup() {
        self.emitter = childNode(withName: "Sound") as! SoundEmitter
        self.light = childNode(withName: "Light") as! SKLightNode
        self.lightSprite = childNode(withName: "LightSprite") as! SKSpriteNode
        self.lightSprite.alpha = 0
    }
    
    func update(deltaTime: TimeInterval) {
        
        currentTime += deltaTime
        if isOn {
            if currentTime >= turnOnTime {
                currentTime -= turnOnTime
                turnOff()
            }
        } else {
            if currentTime >= turnOffTime {
                currentTime -= turnOffTime
                turnOn()
            }
        }
    }
    
    func turnOn() {
        isOn = true
        if emitter.isPlaying {
            light.isEnabled = true
            lightSprite.run(SKAction.fadeIn(withDuration: 0.2))
        }
    }
    
    func turnOff() {
        isOn = false
        if emitter.isPlaying || light.isEnabled {
            light.isEnabled = false
            lightSprite.run(SKAction.fadeOut(withDuration: 0.2))
        }
    }
}
