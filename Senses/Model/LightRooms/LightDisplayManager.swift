//
//  LightDisplayManager.swift
//  Senses
//
//  Created by Rafael Forbeck on 24/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

class LightDisplayManager {
    
    private weak var gameScene: GameScene!
    private var lights = [LightDisplay]()
    
    func setup(gameScene: GameScene) {
        self.gameScene = gameScene
    }
    
    func registerLight(light: LightDisplay, soundsManager: SoundsManager) {
        
        light.setup()
        lights.append(light)
        
        let soundEmitter = light.childNode(withName: "Sound") as! SoundEmitter
        soundsManager.registerSoundEmitter(emitter: soundEmitter, playInLoop: true)
        
        let radioStatic = light.childNode(withName: "Static") as! SoundEmitter
        soundsManager.registerSoundEmitter(emitter: radioStatic, playInLoop: true)
    }
    
    func update(deltaTime: TimeInterval) {
        for light in lights {
            light.update(deltaTime: deltaTime)
        }
    }
}

