//
//  Config.swift
//  Senses
//
//  Created by Rafael Forbeck on 17/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit
import UIKit

class Config {
    
    // Player
    static let playerAngularVelocity = CGFloat(5)
    static let playerMoveForce = CGFloat(300)
    static let playerRunForce = CGFloat(600)
    
    // Sounds
    static let userDataSounodFileName = "SoundFileName"
    
    // Levels
    static let levelsCount = 9
    
    // Puzzle colors
    static func getColor(color: PuzzleLightColor) -> UIColor {
        switch color {
        case .Blue:
            return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        case .Green:
            return #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        case .Red:
            return #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        case .Yellow:
            return #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        }
    }
}
