//
//  Enemy.swift
//  Senses
//
//  Created by Rafael Forbeck on 20/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

class Enemy: SKSpriteNode {
    
    private var smoke : SKSpriteNode!
    private var smokeFrames : [SKTexture]!
    
    var death: CGRect!
    private var velocity = Float(100)
    
    func setup() {
        alpha = 0
        let deathNode = childNode(withName: "Death")!
        deathNode.isHidden = true
        self.death = deathNode.frame
        
        setupMovement()
        
        let smokeAnimatedAtlas = SKTextureAtlas(named: "Smoke")
        self.smokeFrames = [SKTexture]()
        
        let numImages = smokeAnimatedAtlas.textureNames.count
        for i in 0..<numImages {
            let smokeTextureName = "Smoke\(i)"
            self.smokeFrames.append(smokeAnimatedAtlas.textureNamed(smokeTextureName))
        }
        let firstFrame = smokeFrames[0]
        smoke = SKSpriteNode(texture: firstFrame)
        smoke.blendMode = .multiply
        smoke.zPosition = 10
        addChild(smoke)
        smoke.run(SKAction.repeatForever(
            SKAction.animate(with: smokeFrames,
                             timePerFrame: 0.04,
                             resize: false,
                             restore: true)),
                withKey:"smokeAnimation")
    }
    
    func setupMovement() {
        
        let movementTarget = childNode(withName: "MovementTarget") as? SKSpriteNode
        if movementTarget != nil {
            if userData != nil {
                if userData!["Velocity"] != nil {
                    self.velocity = userData!["Velocity"] as! Float
                }
            }
            let distance = (movementTarget!.absolutPosition() - self.position).length()
            let duration = TimeInterval(distance / CGFloat(velocity))
            let movement = SKAction.move(to: movementTarget!.absolutPosition(), duration: duration)
            let backMovement = SKAction.move(to: self.position, duration: duration)
            self.run(SKAction.repeatForever(SKAction.sequence([movement, backMovement])))
        }
    }
}
