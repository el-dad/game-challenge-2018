//
//  EnemiesManager.swift
//  Senses
//
//  Created by Rafael Forbeck on 20/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit
import AVFoundation

class EnemiesManager {
    
    private weak var gameScene: GameScene!
    private var enemies = [Enemy]()
    private var deathSound: AVAudioPlayer!
    
    func setup(gameScene: GameScene, soundsManager: SoundsManager) {
        self.gameScene = gameScene
        let enemies = gameScene.childNode(withName: "Enemies")
        if enemies != nil
        {
            for node in enemies!.children {
                let enemy = node as! Enemy
                enemy.setup()
                self.enemies.append(enemy)
                
                let soundEmitter = enemy.childNode(withName: "Sound") as! SoundEmitter
                soundsManager.registerSoundEmitter(emitter: soundEmitter, playInLoop: true)
            }
        }
    }
    
    func update(playerPosition: CGPoint) {
        for enemy in enemies {
            if enemy.death.contains(playerPosition - enemy.absolutPosition()) {
                deathSound = AudioPlayer.shared.loadSound(fileName: "death.mp3")
                deathSound.volume = 0.4
                deathSound.play()
                enemy.alpha = 1
                enemy.run(SKAction.move(to: playerPosition - enemy.parent!.position, duration: 1))
                self.gameScene.gameOver()
            }
        }
    }
}
