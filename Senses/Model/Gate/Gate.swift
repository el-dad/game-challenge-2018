//
//  Door.swift
//  Senses
//
//  Created by Rafael Forbeck on 06/11/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit
import AVFoundation

class Gate: SKSpriteNode {
    
    private var audio: AVAudioPlayer!
    
    func open() {
        
        audio = AudioPlayer.shared.loadSound(fileName: "open_gate.wav")
        audio.numberOfLoops = -1
        audio.volume = 0.1
        audio.play()
        let duration = TimeInterval(2)
        let displacement = self.frame.height > self.frame.width ? self.frame.height : self.frame.width
        run(SKAction.move(by: CGVector(dx: cos(self.zRotation) * displacement, dy: sin(self.zRotation) * displacement), duration: duration))
        DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
            self.audio.stop()
        })
    }
}
