//
//  SKNode+Extensions.swift
//  Senses
//
//  Created by Rafael Forbeck on 20/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

extension SKNode {
    
    func absolutPosition() -> CGPoint {
        if parent == nil {
            return position
        } else {
            return position + parent!.absolutPosition()
        }
    }
}

