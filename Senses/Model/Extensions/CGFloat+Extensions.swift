//
//  CGFloat+Extensions.swift
//  Senses
//
//  Created by Rafael Forbeck on 17/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

public extension CGFloat {
    
    static func randomBetween(a: CGFloat, b: CGFloat) -> CGFloat
    {
        let range = b - a
        return ((CGFloat(Float(arc4random()) / Float(UINT32_MAX))) * range) + a;
    }
    
    /**
     * Ensures that the float value stays between the given values, inclusive.
     */
    public func clamped(_ v1: CGFloat, _ v2: CGFloat) -> CGFloat {
        let min = v1 < v2 ? v1 : v2
        let max = v1 > v2 ? v1 : v2
        return self < min ? min : (self > max ? max : self)
    }
    
    /**
     * Ensures that the float value stays between the given values, inclusive.
     */
    public mutating func clamp(_ v1: CGFloat, _ v2: CGFloat) -> CGFloat {
        self = clamped(v1, v2)
        return self
    }

    /**
     * Returns the shortest angle between two angles. The result is always between
     * -π and π.
     */
    static func shortestAngleBetween(_ angle1: CGFloat, angle2: CGFloat) -> CGFloat {
        
        var angle = (angle2 - angle1).truncatingRemainder(dividingBy: CGFloat.pi * 2.0)
        if (angle >= CGFloat.pi) {
            angle = angle - CGFloat.pi * 2.0
        }
        if (angle <= -CGFloat.pi) {
            angle = angle + CGFloat.pi * 2.0
        }
        return angle
    }
}
