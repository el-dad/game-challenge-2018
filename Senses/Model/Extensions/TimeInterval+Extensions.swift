//
//  TimeInterval+Extensions.swift
//  Senses
//
//  Created by Rafael Forbeck on 18/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import Foundation

import UIKit

extension TimeInterval {
    
    static func randomBetween(a: TimeInterval, b: TimeInterval) -> TimeInterval
    {
        let range = b - a
        return ((TimeInterval(arc4random()) / TimeInterval(UINT32_MAX)) * range) + a;
    }
}
