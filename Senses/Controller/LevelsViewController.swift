//
//  LevelsViewController.swift
//  Senses
//
//  Created by Rafael Forbeck on 24/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import UIKit

class LevelsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var levelsCollectionView: UICollectionView!
    
    private var levelNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        levelsCollectionView.delegate = self
        levelsCollectionView.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let size = (levelsCollectionView.frame.width / 3) * 0.95
        layout.itemSize = CGSize(width: size, height: size)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = levelsCollectionView.frame.height * 0.02
        levelsCollectionView!.collectionViewLayout = layout
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Config.levelsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = levelsCollectionView.dequeueReusableCell(withReuseIdentifier: "level", for: indexPath) as? LevelCollectionViewCell else { fatalError("CollectionCell inválida") }
        
        cell.setLevelNumber(levelNumber, textHeight: cell.frame.height * 0.3)
        levelNumber += 1
        return cell
    }
    
    @IBAction func levelButtonPressed(_ sender: UIButton) {
        let gameViewController = storyboard?.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        gameViewController.setLevelNumber(sender.tag)
        navigationController?.pushViewController(gameViewController, animated: false)
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: false)
    }
}
