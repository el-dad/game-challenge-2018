//
//  GoalViewController.swift
//  Senses
//
//  Created by Rafael Forbeck on 23/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import UIKit
import AVFoundation

class GoalViewController: UIViewController {

    private weak var gameViewController: GameViewController!
    private var avPlayer: AVPlayer!
    private var avPlayerLayer: AVPlayerLayer!
    private var levelNumber: Int!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var levelsButton: UIButton!
    private var cutSceneSound: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        self.setupButtons()
        
        self.animateViews()
        cutSceneSound = AudioPlayer.shared.loadSound(fileName: "goal_sound.mp3")
        cutSceneSound.volume = 0.3
        cutSceneSound.play()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(_:)), name: .AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem)
        self.avPlayer?.play()
    }

    override func viewDidLayoutSubviews() {
        
        if let theURL = Bundle.main.url(forResource: "cutscene_exit", withExtension: "mp4") {
            
            self.avPlayer = AVPlayer(url: theURL)
            self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
            self.avPlayerLayer.frame = self.videoView.bounds
            self.avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            self.avPlayer.volume = 1
            self.avPlayer.actionAtItemEnd = .none
            
            self.view.backgroundColor = .clear
            self.videoView.layer.insertSublayer(self.avPlayerLayer, at: 1)
            self.avPlayer.play()
        }
        
        self.animateViews()
    }
    
    @objc func playerItemDidReachEnd(_ notification: Notification) {
        // Terminou o vídeo
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
        self.avPlayer?.pause()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setGameViewController(gameViewController: GameViewController) {
        self.gameViewController = gameViewController
    }
    
    private func setupButtons() {
        if levelNumber == Config.levelsCount {
            nextButton.isEnabled = false
            nextButton.alpha = 0
        }
    }
    
    private func animateViews() {
        self.titleView.alpha = 0
        
        let levelsButtonOrigin = levelsButton.frame.origin
        let nextButtonOrigin = nextButton.frame.origin
        
        levelsButton.frame.origin = levelsButton.frame.origin - CGPoint(x: levelsButton.frame.width, y: 0)
        nextButton.frame.origin = nextButton.frame.origin + CGPoint(x: nextButton.frame.width, y: 0)
        
        UIView.animate(withDuration: 5, delay: 5, options: [], animations: {
            self.titleView.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: 0.4, delay: 8, options: [.allowUserInteraction], animations: {
            self.levelsButton.frame.origin = levelsButtonOrigin
            self.nextButton.frame.origin = nextButtonOrigin
        }) { _ in
            self.levelsButton.isUserInteractionEnabled = true
            self.nextButton.isUserInteractionEnabled = true
        }
    }
    
    func setLevel(levelNumber: Int) {
        self.levelNumber = levelNumber
    }
    
    @IBAction func levelsPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: false)
        gameViewController.exit()
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: false)
        gameViewController.setLevelNumber(levelNumber + 1)
        gameViewController.loadScene()
    }
}
