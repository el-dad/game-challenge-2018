//
//  PauseViewController.swift
//  Senses
//
//  Created by Rafael Forbeck on 25/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import UIKit

class PauseViewController: UIViewController {
    
    private var gameViewController: GameViewController!
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var levelsButton: UIButton!
    @IBOutlet weak var resumeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        animateViews()
    }

    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setGameViewController(gameViewController: GameViewController) {
        self.gameViewController = gameViewController
    }
    
    private func animateViews() {
        titleView.alpha = 0
        let levelsButtonOrigin = levelsButton.frame.origin
        let resumeButtonOrigin = resumeButton.frame.origin
        
        levelsButton.frame.origin = levelsButton.frame.origin - CGPoint(x: levelsButton.frame.width, y: 0)
        resumeButton.frame.origin = resumeButton.frame.origin + CGPoint(x: resumeButton.frame.width, y: 0)
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.titleView.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: 0.2, delay: 0.1, options: [.allowUserInteraction], animations: {
            self.levelsButton.frame.origin = levelsButtonOrigin
            self.resumeButton.frame.origin = resumeButtonOrigin
        }, completion: nil)
    }
    
    @IBAction func continuePressed(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.levelsButton.frame.origin = self.levelsButton.frame.origin - CGPoint(x: self.levelsButton.frame.width, y: 0)
            self.resumeButton.frame.origin = self.resumeButton.frame.origin + CGPoint(x: self.resumeButton.frame.width, y: 0)
        }, completion: nil)
        UIView.animate(withDuration: 0.2, delay: 0.1, options: [.allowUserInteraction], animations: {
            self.titleView.alpha = 0
        }) { _ in
            self.gameViewController.pause(pause: false)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func levelsPressed(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.levelsButton.frame.origin = self.levelsButton.frame.origin - CGPoint(x: self.levelsButton.frame.width, y: 0)
            self.resumeButton.frame.origin = self.resumeButton.frame.origin + CGPoint(x: self.resumeButton.frame.width, y: 0)
        }, completion: nil)
        UIView.animate(withDuration: 0.2, delay: 0.1, options: [.allowUserInteraction], animations: {
            self.titleView.alpha = 0
        }) { _ in
            self.gameViewController.exit()
            self.dismiss(animated: false, completion: nil)
        }
    }
}
