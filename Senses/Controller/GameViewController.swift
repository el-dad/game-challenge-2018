//
//  GameViewController.swift
//  Senses
//
//  Created by Rafael Forbeck on 16/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    private var levelNumber: Int = 1
    public weak var gameScene: GameScene!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        loadScene()
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func loadScene() {
        
        if let view = self.view as! SKView? {
            
            AppSingleton.instance.shuffleColors()
            
            if let scene = SKScene(fileNamed: "Level" + levelNumber.description) {
                
                gameScene = scene as! GameScene
                gameScene.setGameController(controller: self)
                scene.scaleMode = .aspectFill
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = false
            view.showsNodeCount = false
            view.showsPhysics = false
        }
    }
    
    func setLevelNumber(_ number: Int) {
        levelNumber = number
    }
    
    func goal() {
        self.performSegue(withIdentifier: "Goal", sender: self)
    }
    
    func gameOver() {
        self.performSegue(withIdentifier: "GameOver", sender: self)
    }
    
    func pause(pause: Bool) {
        gameScene.pause(pause: pause)
    }
    
    func exit() {
        navigationController?.popViewController(animated: false)
    }
    
    func showPuzzle() {
        performSegue(withIdentifier: "Puzzle", sender: self)
    }
    
    func openTheGate() {
        self.gameScene.openTheGate()
    }
    
    @IBAction func pausePressed(_ sender: UIButton) {
        if gameScene.isGameFinished() {
            return
        }
        self.pause(pause: true)
        performSegue(withIdentifier: "Pause", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PauseViewController {
            viewController.setGameViewController(gameViewController: self)
        }
        else if let viewController = segue.destination as? GameOverViewController {
            viewController.setGameViewController(gameViewController: self)
        }
        else if let viewController = segue.destination as? GoalViewController {
            viewController.setLevel(levelNumber: levelNumber)
            viewController.setGameViewController(gameViewController: self)
        }
        else if let viewController = segue.destination as? PuzzleViewController {
            viewController.colorSequence = gameScene.getColorSequence()
            viewController.setGameViewController(gameViewController: self)
        }
    }
}
