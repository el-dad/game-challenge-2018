//
//  GameOverViewController.swift
//  Senses
//
//  Created by Rafael Forbeck on 20/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import UIKit
import AVFoundation

class GameOverViewController: UIViewController {
    
    private weak var gameViewController: GameViewController!
    private var avPlayer: AVPlayer!
    private var avPlayerLayer: AVPlayerLayer!
    private var cutSceneSound: AVAudioPlayer!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var levelsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        cutSceneSound = AudioPlayer.shared.loadSound(fileName: "death_sound.mp3")
        cutSceneSound.volume = 0.5
        cutSceneSound.play()
    }
    
    override func viewDidLayoutSubviews() {
        if let theURL = Bundle.main.url(forResource: "cutscene_death", withExtension: "mp4") {
            
            self.avPlayer = AVPlayer(url: theURL)
            self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
            self.avPlayerLayer.frame = self.videoView.bounds
            self.avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            self.avPlayer.volume = 1
            self.avPlayer.actionAtItemEnd = .none
            
            self.view.backgroundColor = .clear
            self.videoView.layer.insertSublayer(self.avPlayerLayer, at: 1)
            self.avPlayer.play()
        }
        
        self.animateViews()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setGameViewController(gameViewController: GameViewController) {
        self.gameViewController = gameViewController
    }
    
    private func animateViews() {
        titleView.alpha = 0
        let levelsButtonOrigin = levelsButton.frame.origin
        let tryAgainButtonOrigin = tryAgainButton.frame.origin
        
        levelsButton.frame.origin = levelsButton.frame.origin - CGPoint(x: levelsButton.frame.width, y: 0)
        tryAgainButton.frame.origin = tryAgainButton.frame.origin + CGPoint(x: tryAgainButton.frame.width, y: 0)
        
        UIView.animate(withDuration: 3, delay: 4.5, options: [], animations: {
            self.titleView.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: 0.4, delay: 6, options: [.allowUserInteraction], animations: {
            self.levelsButton.frame.origin = levelsButtonOrigin
            self.tryAgainButton.frame.origin = tryAgainButtonOrigin
        }) { _ in
            self.levelsButton.isUserInteractionEnabled = true
            self.tryAgainButton.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func tryAgainPressed(_ sender: UIButton) {
        gameViewController.loadScene()
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func exitButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: false)
        gameViewController.exit()
    }
}
