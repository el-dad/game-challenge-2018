//
//  StartScene.swift
//  Senses
//
//  Created by Rafael Forbeck on 29/11/2017.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit

class TitleScene: SKScene {
    
    private var walkAnimation : SKAction!
    private var playerSprite: SKSpriteNode!
    
    override func didMove(to view: SKView) {
        
        setupAnimation()
    }
    
    private func setupAnimation() {
        
        let walkAnimatedAtlas = SKTextureAtlas(named: "WalkTitle")
        var walkFrames = [SKTexture]()
        
        let numWalkImages = walkAnimatedAtlas.textureNames.count
        for i in 1..<numWalkImages + 1 {
            let walkTextureName = "img_char_walk_up_0\(i)"
            walkFrames.append(walkAnimatedAtlas.textureNamed(walkTextureName))
        }
        
        let firstWalkFrame = walkFrames[0]
        playerSprite = SKSpriteNode(texture: firstWalkFrame)
        playerSprite.zPosition = 2
        playerSprite.position = CGPoint(x: 0, y: -50)
        addChild(playerSprite)
        
        walkAnimation = SKAction.repeatForever(
            SKAction.animate(with: walkFrames,
                             timePerFrame: 0.15,
                             resize: false,
                             restore: true))
        
        playerSprite.run(walkAnimation)
    }
}
