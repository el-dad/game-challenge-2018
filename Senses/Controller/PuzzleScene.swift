//
//  PuzzleScene.swift
//  Senses
//
//  Created by Eldade Marcelino on 20/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import AVFoundation
import SpriteKit

class PuzzleScene: SKScene {
    
    private lazy var buttons: [SKSpriteNode]? = {
        let circle = self.childNode(withName: "Circle")
        return circle?.children.map({ node -> SKSpriteNode in
            return node as! SKSpriteNode
        })
    }()
    private var answer = [PuzzleLightColor?]()
    private lazy var lamps: [SKSpriteNode]? = {
        let lamps = self.childNode(withName: "Lamps")
        return lamps?.children.map({ node -> SKSpriteNode in
            return node as! SKSpriteNode
        })
    }()
    private lazy var lampIterator = {
       return self.lamps?.makeIterator()
    }()
    private lazy var screenItems: [SKNode]? = {
        return self.childNode(withName: "Screen")?.children
    }()
    private lazy var screenItemsIterator = {
        return self.screenItems?.makeIterator()
    }()
    private lazy var screenText = {
        return self.childNode(withName: "screenText") as? SKLabelNode
    }()
    public var generalSfx = ["error", "success", "processing"].map { audio -> AVAudioPlayer in
        let pl = AudioPlayer.shared.loadSound(fileName: "puzzle-\(audio).mp3")
        pl.volume = 0.2
        return pl
    }
    private var soundSequence = [AVAudioPlayer]()
    public weak var gameController: GameViewController? = nil
    private var _colorSequence = [PuzzleLightColor]()
    public weak var sceneController: UIViewController?
    public var colorSequence : [PuzzleLightColor] {
        get {
            return self._colorSequence
        }
        set {
            self._colorSequence = newValue
            self.soundSequence = newValue.map({ color -> AVAudioPlayer in
                let audio = AudioPlayer.shared.loadSound(fileName: AppSingleton.instance.colorConfusionDictionary["sound-puzzle-\(color.rawValue).mp3"]!)
                audio.delegate = self
                return audio
            })
            self.soundsIterator = self.soundSequence.makeIterator()
            self.playSounds()
            self.colorSequenceIterator = self._colorSequence.makeIterator()
        }
    }
    private var colorSequenceIterator: IndexingIterator<[PuzzleLightColor]>? = nil
    private var soundsIterator = [AVAudioPlayer]().makeIterator()
    
    private func screenAnimation() {
        self.screenItems?.forEach({ item in
            item.alpha = 0
        })
        
        if self.animationHold {
            self.screenText?.alpha = 1
            self.screenItemsIterator = self.screenItems?.makeIterator()
            self.verifySequence()
            self.animationHold = false
            return
        }
        
        if let frame = self.screenItemsIterator?.next() {
            frame.alpha = 1
            self.run(SKAction.wait(forDuration: 0.2)) {
                self.screenAnimation()
            }
        } else {
            self.screenItemsIterator = self.screenItems?.makeIterator()
            self.screenAnimation()
        }
    }
    
    private var animationHold = false
    
    private func playSounds() {
        if let sound = self.soundsIterator.next() {
            sound.volume = 0.2
            sound.playAndSave(scene: self)
        } else {
            self.soundsIterator = self.soundSequence.makeIterator()
            self.run(SKAction.wait(forDuration: 2)) {
                if self.generalSfx.first(where: { player -> Bool in
                    return player.isPlaying
                }) != nil {
                    let lol = self.sfxHandler?.finishedPlayingCallback
                    self.sfxHandler?.finishedPlayingCallback = {
                        lol?()
                        self.playSounds()
                    }
                } else {
                    self.playSounds()
                }
            }
        }
    }
    
    override func didMove(to view: SKView) {
        self.screenText?.fontName = "Digital tech"
    }
    
    private func getButtonByTouches(touches: Set<UITouch>) -> SKSpriteNode? {
        if let touch = touches.first {
            let button = self.buttons?.first(where: { button -> Bool in
                return button.contains(touch.location(in: self))
            })
            return button
        }
        return nil
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let button = self.getButtonByTouches(touches: touches)
        button?.texture = SKTexture(imageNamed: "\(button!.name!)_pressed")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let button = self.getButtonByTouches(touches: touches) {
            let buttonColor = "\(button.name!.split(separator: "_")[1])"
            button.texture = SKTexture(imageNamed: "\(button.name!)_normal")
            
            if self.screenText?.text != "INSERT CODE" {
                return
            }
            
            self.answer.append(PuzzleLightColor(rawValue: buttonColor))
            self.lampIterator?.next()?.texture = SKTexture(imageNamed: "img_lamp_on")
            
            if self.answer.count == self.colorSequence.count {
                self.screenAnimation()
                self.screenText?.alpha = 0
                self.currentPlayingSound = self.soundSequence.first(where: { player -> Bool in
                    return player.isPlaying
                })
                self.generalSfx[2].delegate = self.sfxHandler
                self.currentPlayingSound?.pause()
                self.generalSfx[2].playAndSave(scene: self)
                self.sfxHandler?.finishedPlayingCallback = {
                    self.animationHold = true
                    self.sfxHandler?.finishedPlayingCallback = {}
                    self.generalSfx[2].delegate = nil
                }
                
                return
            }
        }
    }
    
    public var sfxHandler: SimpleAudioDelegate?
    private var currentPlayingSound: AVAudioPlayer?
    
    private func verifySequence () {
        let pass = self.colorSequence.enumerated().first(where: { (key, value) -> Bool in
            return self.answer[key]?.rawValue != value.rawValue
        }) == nil
        
        self.answer = [PuzzleLightColor?]()
        
        if pass {
            self.generalSfx[1].playAndSave(scene: self)
            self.screenText?.text = "SUCCESS"
            self.run(SKAction.wait(forDuration: 2)) {
                self.gameController?.gameScene.exit(delay: 0.0)
                self.gameController?.gameScene.puzzleZone = nil
                (self.sceneController as? PuzzleViewController)?.leaveScene()
            }
        } else {
            self.generalSfx.forEach { player in
                player.delegate = self.sfxHandler
            }
            self.sfxHandler?.finishedPlayingCallback = {
                self.currentPlayingSound?.playAndSave(scene: self)
            }
            self.generalSfx[0].playAndSave(scene: self)
            self.screenText?.text = "ERROR"
            self.lampIterator = self.lamps?.makeIterator()
            self.colorSequenceIterator = self.colorSequence.makeIterator()
            self.blinkLamps(sequence: ["off", "on", "off", "on", "off", "on", "off"])
            self.run(SKAction.wait(forDuration: 2)) {
                self.screenText?.text = "INSERT CODE"
            }
        }
    }
    
    private func blinkLamps(sequence: [String], timePerFrame: TimeInterval = 0.2) {
        self.lamps?.forEach({ lamp in
            lamp.run(SKAction.animate(with: sequence.map({ state -> SKTexture in
                return SKTexture(imageNamed: "img_lamp_\(state)")
            }), timePerFrame: 0.2))
        })
    }
    
    public var playingSound: AVAudioPlayer?
}

extension PuzzleScene: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        try? self.playSounds()
    }
}

extension AVAudioPlayer {
    func playAndSave(scene: PuzzleScene) {
        scene.playingSound = self
        self.play()
    }
}
