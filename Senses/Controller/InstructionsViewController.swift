//
//  InstructionsViewController.swift
//  Senses
//
//  Created by Rafael Forbeck on 01/11/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import UIKit
import AVFoundation

class InstructionsViewController: UIViewController {

    private var avPlayer: AVPlayer!
    private var avPlayerLayer: AVPlayerLayer!
    @IBOutlet weak var videoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        if let theURL = Bundle.main.url(forResource: "senses_tutorial", withExtension: "mp4") {
            
            self.avPlayer = AVPlayer(url: theURL)
            self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
            self.avPlayerLayer.frame = self.videoView.bounds
            self.avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            self.avPlayer.volume = 1
            self.avPlayer.actionAtItemEnd = .none
            
            self.view.backgroundColor = .clear
            self.videoView.layer.insertSublayer(self.avPlayerLayer, at: 1)
            self.avPlayer.play()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(_:)), name: .AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    @objc func playerItemDidReachEnd(_ notification: Notification) {
        self.avPlayer.seek(to: kCMTimeZero)
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
