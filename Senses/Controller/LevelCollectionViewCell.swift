//
//  LevelCollectionViewCell.swift
//  Senses
//
//  Created by Rafael Forbeck on 24/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import UIKit

class LevelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var levelButton: UIButton!
    
    func setLevelNumber(_ number: Int, textHeight: CGFloat) {
        levelButton.tag = number
        levelButton.setTitle(number.description, for: .normal)
        levelButton.titleLabel?.font = UIFont(name: levelButton.titleLabel!.font.fontName, size: textHeight)
    }
}
