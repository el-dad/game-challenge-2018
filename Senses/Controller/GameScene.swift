//
//  GameScene.swift
//  Senses
//
//  Created by Rafael Forbeck on 16/10/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameScene: SKScene {
    
    private var lastUpdateTime = TimeInterval()
    private weak var gameViewController: GameViewController!
    private var player: Player!
    private var background: SKSpriteNode!
    private var soundsManager = SoundsManager()
    private var enemiesManager = EnemiesManager()
    private var lightsDisplayManager = LightDisplayManager()
    private var exitNode: SKSpriteNode!
    public var puzzleZone: SKSpriteNode!
    private var gateTrigger: SKSpriteNode!
    private var gate: Gate!
    private var isFinished = false
    private var backFromPause = false
    private var inPuzzleZone = false
    private var inGateTrigget = false
    private var ambientSounds = [AVAudioPlayer]()
    private var colorSequence: [PuzzleLightColor] = {
        var new = [PuzzleLightColor]()
        var old = [PuzzleLightColor.Red, PuzzleLightColor.Green, PuzzleLightColor.Blue, PuzzleLightColor.Yellow]
        repeat {
            new.append(old.remove(at: Int(arc4random() % UInt32(old.count))))
        } while(old.count > 0)
        return new
    }()
    private var waters = Waters()
    private var scream: AVAudioPlayer!
    private var fog : SKSpriteNode!
    private var fogFrames : [SKTexture]!
    private var doubleTapTime = TimeInterval(0.3)
    private var lastTapTime = TimeInterval(0.0)
    
    override func didMove(to view: SKView) {
        
        self.background = childNode(withName: "Background") as! SKSpriteNode
        self.exitNode = childNode(withName: "Exit") as? SKSpriteNode
        self.puzzleZone = childNode(withName: "PuzzleZone") as? SKSpriteNode
        if self.puzzleZone != nil {
            self.puzzleZone.alpha = 0
        }
        self.gateTrigger = childNode(withName: "GateTrigger") as? SKSpriteNode
        if self.gateTrigger != nil {
            self.gateTrigger.alpha = 0
        }
        
        self.gate = childNode(withName: "Gate") as? Gate
        self.player = childNode(withName: "Player") as! Player
        self.player.setupPhysicBody()
        self.enemiesManager.setup(gameScene: self, soundsManager: soundsManager)
        self.lightsDisplayManager.setup(gameScene: self)
        self.setupBackground()
        self.setupLightsDisplay()
        self.setupCamera()
        self.setupSounds()
        self.setupWater()
    }
    
    func setGameController(controller: GameViewController) {
        self.gameViewController = controller
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let run: Bool
        if lastUpdateTime - lastTapTime < doubleTapTime {
            run = true
        } else {
            run = false
        }
        lastTapTime = lastUpdateTime
        for touch in touches {
            player.move(inDirection: touch.location(in: self), run: run)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            player.move(inDirection: touch.location(in: self))
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        player.stop()
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        var deltaTime: TimeInterval
        if self.lastUpdateTime == 0.0 {
            deltaTime = 0
        } else {
            deltaTime = currentTime - self.lastUpdateTime
        }
        lastUpdateTime = currentTime
        
        if backFromPause {
            deltaTime = TimeInterval(0.0)
            backFromPause = false
        }
        
        player.update(deltaTime: deltaTime)
        player.walkOnWater(isWalkingOnWater: waters.isPlayerOnTheWater(playerPosition: player.position))
        
        if !isFinished {
            soundsManager.update(deltaTime: deltaTime, playerPosition: player.position, playerDirection: player.zRotation)
            enemiesManager.update(playerPosition: player.position)
            lightsDisplayManager.update(deltaTime: deltaTime)
            if puzzleZone != nil {
                if !inPuzzleZone {
                    if puzzleZone.contains(player.position) {
                        inPuzzleZone = true
                        startPuzzle()
                    }
                } else {
                    if !puzzleZone.contains(player.position) {
                        inPuzzleZone = false
                    }
                }
            }
            if gateTrigger != nil {
                if !inGateTrigget {
                    if gateTrigger.contains(player.position) {
                        inGateTrigget = true
                        openTheGate()
                    }
                }
            }
            if exitNode != nil {
                if exitNode.frame.contains(player.position) {
                    exit()
                }
            }
        }
    }
    
    func startPuzzle() {
        player.stop()
        gameViewController.showPuzzle()
    }
    
    func setupLightsDisplay() {
        enumerateChildNodes(withName: "LightDisplay") { (node, _) in
            let light = node as! LightDisplay
            self.lightsDisplayManager.registerLight(light: light, soundsManager: self.soundsManager)
        }
    }
    
    func setupCamera() {
        
        guard let camera = camera else {
            return
        }
        
        let zeroDistance = SKRange(constantValue: 0)
        let heroContraint = SKConstraint.distance(zeroDistance, to: player)
        
        let xRange = SKRange(lowerLimit: -background.frame.width / 2 + frame.width / 2 * camera.xScale, upperLimit: background.frame.width / 2 - frame.width / 2 * camera.xScale)
        let yRange = SKRange(lowerLimit: -background.frame.height / 2 + frame.height / 2 * camera.yScale, upperLimit: background.frame.height / 2 - frame.height / 2 * camera.xScale)
        
        let edgeConstraint = SKConstraint.positionX(xRange, y: yRange)
        
        edgeConstraint.referenceNode = background
        
        camera.constraints = [heroContraint, edgeConstraint]
    }
    
    func setupBackground() {
        
        let tileSize: Int = 256
        
        let cols: Int = Int(background.frame.width / CGFloat(tileSize))
        let rows: Int = Int(background.frame.height / CGFloat(tileSize))
        
        let backgoundZPosition = background.zPosition
        
        for col in 0 ... cols {
            for row in 0 ... rows {
                let tileNumber = arc4random_uniform(3) + 1
                let sprite = SKSpriteNode(imageNamed: "img_ground_tile_0\(tileNumber)")
                sprite.position = background.position - CGPoint(x: background.frame.width * 0.5, y: background.frame.height * 0.5) + CGPoint(x: col * tileSize, y: row * tileSize)
                sprite.lightingBitMask = 3
                sprite.zPosition = backgoundZPosition
                addChild(sprite)
            }
        }
    }
    
    func setupSounds() {
        let sounds = childNode(withName: "Sounds")
        sounds?.enumerateChildNodes(withName: "Sound") { (node, _) in
            let sound = node as! SoundEmitter
            self.soundsManager.registerSoundEmitter(emitter: sound, playInLoop: true)
        }
        
        let backgroundSound = AudioPlayer.shared.loadSound(fileName: "background.mp3")
        backgroundSound.volume = 0.4
        backgroundSound.numberOfLoops = -1
        backgroundSound.play()
        ambientSounds.append(backgroundSound)
    }
    
    func setupWater() {
        let waterProps = childNode(withName: "GroundProps")
        waterProps?.enumerateChildNodes(withName: "Water", using: { (waterNode, _) in
            self.waters.registerWater(water: waterNode as! SKSpriteNode)
        })
    }
    
    func getColorSequence() -> [PuzzleLightColor] {
        return colorSequence
    }
    
    func openTheGate() {
        if self.gate != nil {
            gate.open()
        }
    }
    
    func exit(delay: TimeInterval = 1.5) {
        finish()
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
            self.gameViewController.goal()
        })
    }
    
    func gameOver() {
//        scream = AudioPlayer.shared.loadSound(fileName: "scream.mp3")
//        scream.volume = 0.05
//        scream.play()
        finish()
        player.run(SKAction.fadeAlpha(to: 0.1, duration: 1))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.gameViewController.gameOver()
        })
    }
    
    func finish() {
        isFinished = true
        soundsManager.finish()
        player.finish()
        for sound in ambientSounds {
            sound.stop()
        }
    }
    
    func isGameFinished() -> Bool {
        return isFinished
    }
    
    func pause(pause: Bool) {
        isPaused = pause
        backFromPause = true
        soundsManager.pause(pause: pause)
        player.pause(pause: pause)
        for sound in ambientSounds {
            if pause {
                sound.pause()
            } else {
                sound.play()
            }
        }
    }
}
