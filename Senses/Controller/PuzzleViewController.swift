//
//  PuzzleViewController.swift
//  Senses
//
//  Created by Rafael Forbeck on 06/11/17.
//  Copyright © 2017 Rafael Forbeck. All rights reserved.
//

import SpriteKit
import AVFoundation

class PuzzleViewController: UIViewController {
    
    private weak var gameViewController: GameViewController!
    public var colorSequence = [PuzzleLightColor]()
    private var sfxHandler: SimpleAudioDelegate? = SimpleAudioDelegate()
    private var scene: PuzzleScene?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        
        loadScene()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setGameViewController(gameViewController: GameViewController) {
        self.gameViewController = gameViewController
    }
    
    func loadScene() {
        
        if let view = self.view as! SKView? {
            
            let scene = SKScene(fileNamed: "PuzzleScene") as! PuzzleScene
            scene.sfxHandler = self.sfxHandler
            scene.colorSequence = colorSequence
            scene.scaleMode = .aspectFill
            view.presentScene(scene)
            
            view.ignoresSiblingOrder = true
            self.scene = scene
            view.showsFPS = false
            view.showsNodeCount = false
            view.showsPhysics = false
            scene.gameController = self.gameViewController
            scene.sceneController = self
        }
    }
    
    func leaveScene() {
        self.scene?.playingSound?.pause()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func leaveButtonPressed(_ sender: UIButton) {
        self.leaveScene()
    }
}

public class SimpleAudioDelegate: NSObject, AVAudioPlayerDelegate {
    public var finishedPlayingCallback = {
        
    }
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.finishedPlayingCallback()
    }
}
